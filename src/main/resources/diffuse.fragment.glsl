#version 120 // 330 core

#ifdef GL_ES
precision lowp float;
#define MED mediump
#else
#define MED
#endif

uniform sampler2D u_texture;
uniform  vec4 ambient;

varying MED vec2 v_texCoords;

void main() {
    gl_FragColor.rgb = (ambient.rgb + texture2D(u_texture, v_texCoords).rgb);
    gl_FragColor.a = 1.0;
}