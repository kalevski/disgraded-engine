#version 120

attribute vec4 a_position;
attribute vec2 a_texCoord;

uniform vec2  u_dir;
uniform vec2 u_fboSize;

varying vec2 v_texCoords0;
varying vec2 v_texCoords1;
varying vec2 v_texCoords2;
varying vec2 v_texCoords3;
varying vec2 v_texCoords4;

void main() {
    const vec2 futher = vec2((3.2307692308 / u_fboSize.x), (3.2307692308 / u_fboSize.y) );
    const vec2 closer = vec2(1.3846153846 / u_fboSize.x, 1.3846153846 / u_fboSize.y );

    vec2 f = futher.xy * dir.xy;
    vec2 c = closer.xy * dir.xy;
    v_texCoords0 = a_texCoord - f;
    v_texCoords1 = a_texCoord - c;
    v_texCoords2 = a_texCoord;
    v_texCoords3 = a_texCoord + c;
    v_texCoords4 = a_texCoord + f;

    gl_Position = a_position;
}
