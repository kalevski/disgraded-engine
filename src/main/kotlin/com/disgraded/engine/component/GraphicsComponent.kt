package com.disgraded.engine.component

import com.badlogic.gdx.math.Vector2
import com.disgraded.engine.adapter.Component
import com.disgraded.engine.adapter.Entity
import com.disgraded.engine.util.graphics.Color
import com.disgraded.engine.util.graphics.GraphicAdapter
import com.disgraded.engine.util.graphics.Group
import com.disgraded.engine.util.graphics.RenderingUnit
import com.sun.org.apache.xpath.internal.operations.Bool
import java.lang.Exception

class GraphicsComponent() : Component() {

    private val adapters : ArrayList<GraphicAdapter> = arrayListOf()
    private val units : ArrayList<RenderingUnit> = arrayListOf()

    var anchor : Vector2 = Vector2(.5f, .5f)
    var scale : Vector2 = Vector2(1f, 1f)
    var origin : Vector2 = Vector2(0f, 0f)
    var flipX : Boolean = false
    var flipY : Boolean = false
    var zOrder : Int = 0
    var alpha : Float = 1f
    var tint : Color? = null
    var visible = true

    private var group : Group? = null

    constructor(anchor : Vector2, scale : Vector2, origin : Vector2,
                flipX : Boolean, flipY : Boolean, zOrder : Int) : this() {
        this.anchor = anchor
        this.scale = scale
        this.origin = origin
        this.flipX = flipX
        this.flipY = flipY
        this.zOrder = zOrder
    }

    override fun onAdd(entity: Entity) : Boolean {
        return true
    }

    override fun onRemove(entity: Entity) : Boolean {
        return true
    }

    fun getGraphicAdapters() : ArrayList<GraphicAdapter> {
        return adapters
    }

    fun addGraphicAdapter(adapter: GraphicAdapter) {
        adapters.add(adapter)
    }

    fun removeGraphicAdapter(adapter: GraphicAdapter) {
        adapters.remove(adapter)
    }

    fun hasGraphicAdapter(adapter: GraphicAdapter) : Boolean {
        return adapters.contains(adapter)
    }

    fun removeAllGraphicAdapters() {
        adapters.clear()
    }

    fun getRenderingUnits() : ArrayList<RenderingUnit> {
        units.clear()
        for (adapter in adapters) {
            for (renderingUnit in adapter.getRenderingUnits()) {
                units.add(renderingUnit)
            }
        }
        return units
    }

    fun getRenderOrder(): Long {
        var groupPriority : Int = 0;
        if (group !== null) {
            groupPriority = group!!.priority
        }
        return zOrder.toLong() + (groupPriority.toLong() * Integer.MAX_VALUE)
    }

    fun addToGroup(groupName : String) {
        group = root.entity.getGroup(groupName)
    }

    fun getGroup() : Group? {
        return group
    }

}