package com.disgraded.engine.component

import com.badlogic.gdx.math.Vector2
import com.badlogic.gdx.physics.box2d.*
import com.disgraded.engine.adapter.Component
import com.disgraded.engine.adapter.Entity
import com.disgraded.engine.util.physics.Physics2DDef
import com.disgraded.engine.util.physics.Fixture2DDef

class PhysicsComponent(private var def : Physics2DDef) : Component() {

    private lateinit var entity : Entity

    private var body : Body? = null
    private var fixtureMap : HashMap<String, Fixture> = HashMap()

    var initialized = false

    override fun onAdd(entity: Entity) : Boolean {
        this.entity = entity
        val world = root.physics.getWorld()
        body = world.createBody(BodyDef().apply {
            active = def.active
            allowSleep = def.allowSleep
            angle = def.angle
            angularDamping = def.angularDamping
            angularVelocity = def.angularVelocity
            awake = def.awake
            bullet = def.bullet
            fixedRotation = def.fixedRotation
            gravityScale = def.gravityScale
            linearDamping = def.linearDamping
            type = def.bodyType
        })
        return true
    }

    override fun onRemove(entity: Entity) : Boolean {
        val world = root.physics.getWorld()
        world.destroyBody(body)
        return true
    }

    fun getBody() : Body {
        return body!!
    }

    fun addFixture(fixtureDef : Fixture2DDef) : String {
        fixtureDef.generateFixture()
        val fixture = body!!.createFixture(fixtureDef.getFixtureDef())
        fixture.userData = entity
        fixture.filterData = fixtureDef.getFilter()
        fixtureMap[fixtureDef.getSignature()] = fixture
        return fixtureDef.getSignature()
    }

    fun removeFixture(signature : String) {
        body!!.destroyFixture(fixtureMap[signature])
        fixtureMap.remove(signature)
    }

    fun getFixture(signature : String) : Fixture {
        return fixtureMap[signature]!!
    }

    fun setTransform(position : Vector2, angle : Float) {
        val pixelRatio = root.config.graphics.pixelRatio
        val newPos = Vector2(position.x / pixelRatio, position.y / pixelRatio)
        val rotation = (angle * (Math.PI / 180f)).toFloat()
        body!!.setTransform(newPos, rotation)
    }
}