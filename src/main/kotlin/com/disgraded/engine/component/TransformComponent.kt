package com.disgraded.engine.component

import com.badlogic.gdx.math.Vector2
import com.disgraded.engine.adapter.Component
import com.disgraded.engine.adapter.Entity

class TransformComponent() : Component() {

    var position : Vector2 = Vector2(0f, 0f)
    var rotation : Float = 0f

    constructor(position : Vector2) : this() {
        this.position = position
    }

    constructor(position : Vector2, rotation : Float) : this() {
        this.position = position
        this.rotation = rotation
    }

    override fun onAdd(entity: Entity) : Boolean {
        return true
    }

    override fun onRemove(entity: Entity) : Boolean {
        return true
    }
}