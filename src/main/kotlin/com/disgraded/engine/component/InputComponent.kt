package com.disgraded.engine.component

import com.badlogic.ashley.signals.Signal
import com.badlogic.gdx.graphics.Pixmap
import com.badlogic.gdx.math.Vector2
import com.disgraded.engine.manager.InputManager
import com.disgraded.engine.adapter.Component
import com.disgraded.engine.adapter.Entity
import com.disgraded.engine.util.input.InputShape
import com.sun.org.apache.xpath.internal.operations.Bool
import jdk.internal.util.xml.impl.Input

class InputComponent(private val shape : InputShape) : Component() {

    var isTouched = false

    val onTouchDown : Signal<InputManager.Touch> = Signal()
    val onTouchUp : Signal<InputManager.Touch> = Signal()
    val onTouch : Signal<InputManager.Touch> = Signal()

    var priority : Int = 0
    var inputEnabled : Boolean = true

    override fun onAdd(entity: Entity): Boolean {
        return true
    }

    override fun onRemove(entity: Entity): Boolean {
        return true
    }

    fun getShape() : InputShape {
        return shape
    }
}