package com.disgraded.engine.component

import com.disgraded.engine.adapter.Component
import com.disgraded.engine.adapter.Entity
import com.disgraded.engine.util.Tag
import com.disgraded.engine.util.graphics.Group
import java.util.*
import kotlin.collections.ArrayList

class CoreComponent : Component() {

    val uid : String = UUID.randomUUID().toString()
    var name: String? = null
    private val tagList : ArrayList<Tag> = arrayListOf()

    override fun onAdd(entity: Entity) : Boolean {
        return true
    }

    override fun onRemove(entity: Entity) : Boolean {
        return true
    }

    fun addTag(tagName : String) {
        tagList.add(root.entity.getTag(tagName))
    }

    fun removeTag(tagName : String) {
        tagList.remove(root.entity.getTag(tagName))
    }

    fun hasTag(tagName : String) : Boolean {
        return tagList.contains(root.entity.getTag(tagName))
    }

    fun getTagList() : ArrayList<Tag> {
        return tagList
    }
}