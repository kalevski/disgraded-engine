package com.disgraded.engine

import com.badlogic.gdx.Gdx
import com.disgraded.engine.manager.*
import com.disgraded.engine.util.Config

class Root(private val engine: Engine) {

    val entity : EntityManager = EntityManager()
    val stage : StageManager = StageManager()
    val debug : DebugManager = DebugManager(engine)
    val input : InputManager = InputManager(engine)
    val physics : PhysicsManager = PhysicsManager(engine)
    val task : TaskManager = TaskManager(engine)

    private lateinit var defaultConfig : Config
    var config : Config = Config()

    fun init(config: Config) {
        stage.init()
        defaultConfig = config

        stage.initStage.add { _, cfg -> configureStage(cfg)}
        stage.disposeStage.add { _, _ -> disposeStage() }

//        val a = ShaderProgram(Gdx.files.internal(""), Gdx.files.external(""))
//        a.begin()
//
//        a.end()

        var fileContent = javaClass.getResource("/diffuse.fragment.glsl").readText()
        println(fileContent)
    }

    fun update(deltaTime : Float) {
        engine.graphics.update(deltaTime)
        debug.begin("stage")
        stage.update()
        debug.end("stage")
        input.update()
        entity.update(deltaTime)
        debug.update()
        debug.begin("task")
        task.update()
        debug.end("task")
    }

    fun dispose() {
        stage.dispose()
    }

    private fun configureStage(config: Config?) {
        this.config = this.defaultConfig
        if (config !== null) {
            this.config = config
        }
        engine.graphics.init()
        engine.physics.init()
        entity.init()
        debug.init()
        input.init()
        task.init()
        println("Stage initialized")
    }

    private fun disposeStage() {
        task.dispose()
        debug.printEngineStats()
        input.dispose()
        debug.dispose()
        entity.dispose()
        engine.physics.dispose()
        engine.graphics.dispose()
        println("Stage disposed")
    }

    fun getFPS() : Int {
        return Gdx.graphics.framesPerSecond
    }

    fun getDeltaTime() : Float {
        return Gdx.graphics.deltaTime
    }
}