package com.disgraded.engine.manager

import com.disgraded.engine.Engine
import com.disgraded.engine.util.task.*
import java.lang.Exception
import java.util.*
import java.util.concurrent.BlockingDeque
import java.util.concurrent.LinkedBlockingDeque

class TaskManager(private val engine: Engine) {

    private val asyncTaskMap : HashMap<String, Class<out Task>> = hashMapOf()
    private val syncTaskMap : HashMap<String, Class<out Task>> = hashMapOf()

    private val asyncQueue : BlockingDeque<TaskInvoker> = LinkedBlockingDeque()
    private val syncQueue : Queue<TaskInvoker> = LinkedList()

    private lateinit var asyncWorker : AsyncTaskWorker
    private lateinit var syncWorker : SyncTaskWorker

    fun init() {
        asyncWorker = AsyncTaskWorker(asyncQueue, engine)
        syncWorker = SyncTaskWorker(syncQueue, engine)

        Thread(asyncWorker).start()
    }

    fun update() {
        syncWorker.update()
    }

    fun dispose() {
        asyncTaskMap.clear()
        syncTaskMap.clear()
        asyncQueue.clear()
        syncQueue.clear()
        asyncWorker.stop()
    }

    fun register(key : String, taskClass : Class<out Task>, async : Boolean = false) {
        val keyExist = asyncTaskMap.containsKey(key) || syncTaskMap.containsKey(key)
        if (keyExist) throw Exception("Task $key already exist in task register")
        if (!async) {
            syncTaskMap[key] = taskClass
        } else {
            asyncTaskMap[key] = taskClass
        }
    }

    fun run(key: String, data: Any? = null) {
        if (syncTaskMap.containsKey(key)) {
            val instance = syncTaskMap[key]!!.newInstance()
            syncQueue.add(TaskInvoker(instance, data))
        } else if (asyncTaskMap.containsKey(key)) {
            val instance = asyncTaskMap[key]!!.newInstance()
            asyncQueue.put(TaskInvoker(instance, data))
        } else {
            throw Exception("Task $key doesn't exist in task register")
        }
    }
}