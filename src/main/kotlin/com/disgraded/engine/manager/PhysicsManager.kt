package com.disgraded.engine.manager

import com.badlogic.gdx.physics.box2d.World
import com.disgraded.engine.Engine

class PhysicsManager(private val engine: Engine) {

    fun getWorld() : World {
        return engine.physics.world
    }
}