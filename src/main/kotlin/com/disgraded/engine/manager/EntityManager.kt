package com.disgraded.engine.manager

import com.badlogic.ashley.core.EntitySystem
import com.badlogic.ashley.core.Family
import com.disgraded.engine.component.CoreComponent
import com.disgraded.engine.adapter.Entity
import com.disgraded.engine.adapter.EntityListener
import com.disgraded.engine.component.GraphicsComponent
import com.disgraded.engine.system.*
import com.disgraded.engine.util.Tag
import com.disgraded.engine.util.graphics.Group
import java.lang.Exception

class EntityManager {

    private val ashleyEngine : com.badlogic.ashley.core.Engine = com.badlogic.ashley.core.Engine()
    private val groupMap : HashMap<String, Group> = hashMapOf()
    private val tagMap : HashMap<String, Tag> = hashMapOf()

    fun init() {
        addSystem(RenderingSystem())
        addSystem(DebugSystem())
        addSystem(InputSystem())
        addSystem(PhysicsSystem())
        addSystem(ObjectSystem())
    }

    fun add(entity: Entity) {
        val core = entity.getComponent(CoreComponent::class.java)
        if (core === null) {
            throw Exception("Every entity must contain core component")
        }
        ashleyEngine.addEntity(entity)
        entity.build()
    }

    fun remove(entity: Entity) {
        ashleyEngine.removeEntity(entity)
        entity.destroy()
    }

    fun findByGroup(groupName : String) : List<Entity> {
        if (groupMap[groupName] == null) {
            throw Exception("Group with group name: $groupName doesn't exist")
        }
        val entityList : ArrayList<Entity> = arrayListOf()
        for (entity in ashleyEngine.entities) {
            val graphics = entity.getComponent(GraphicsComponent::class.java)
            if (graphics === null) continue
            if (graphics.getGroup() === null) continue
            if (graphics.getGroup()!!.name == groupMap[groupName]!!.name) {
                entityList.add(entity as Entity)
            }
        }
        return entityList
    }

    fun findByName(name : String) : Entity? {
        for (entity in ashleyEngine.entities) {
            val core = entity.getComponent(CoreComponent::class.java)
            if (core!!.name === name) {
                return entity as Entity
            }
        }
        return null
    }

    fun findByTag(tagName : String) : List<Entity> {
        if (tagMap[tagName] == null) {
            throw Exception("Tag with tag name: $tagName doesn't exist")
        }
        val entityList : ArrayList<Entity> = arrayListOf()
        for (entity in ashleyEngine.entities) {
            val core = entity.getComponent(CoreComponent::class.java)
            if (core!!.hasTag(tagName)) {
                entityList.add(entity as Entity)
            }
        }
        return entityList
    }

    fun findByUID(uid : String) : Entity? {
        for (entity in ashleyEngine.entities) {
            val core = entity.getComponent(CoreComponent::class.java)
            if (core!!.uid === uid) {
                return entity as Entity
            }
        }
        return null
    }

    fun addSystem(system : EntitySystem) {
        ashleyEngine.addSystem(system)
    }

    fun addListener(family: Family, priority : Int, listener : EntityListener) {
        ashleyEngine.addEntityListener(family, priority, listener)
    }

    fun removeListener(listener: EntityListener) {
        ashleyEngine.removeEntityListener(listener)
    }

    fun update(deltaTime : Float) {
        ashleyEngine.update(deltaTime)
    }

    fun dispose() {
        ashleyEngine.removeAllEntities()
        for (system in ashleyEngine.systems) {
            ashleyEngine.removeSystem(system)
        }
        groupMap.clear()
    }

    fun getGroup(groupName: String): Group {
        if (groupMap[groupName] === null) {
            throw Exception("Group $groupName doesn't exist")
        } else {
            return groupMap[groupName]!!
        }
    }

    fun createGroup(groupName : String, priority : Int) {
        if (groupMap[groupName] == null) {
            groupMap[groupName] = Group(groupName, priority)
        } else {
            throw Exception("Group $groupName already exist!")
        }
    }

    fun getTag(tagName: String): Tag {
        if (tagMap[tagName] === null) {
            throw Exception("Tag $tagName doesn't exist")
        } else {
            return tagMap[tagName]!!
        }
    }

    fun createTag(tagName : String) {
        if (tagMap[tagName] == null) {
            tagMap[tagName] = Tag(tagName)
        } else {
            throw Exception("Tag $tagName already exist!")
        }
    }

}
