package com.disgraded.engine.manager

import com.badlogic.ashley.signals.Signal
import com.disgraded.engine.Engine
import com.disgraded.engine.unit.Stage
import com.disgraded.engine.util.Config
import java.lang.Exception

class StageManager {

    private var stageMap: MutableMap<String, Stage> = HashMap()
    private var currentStage: Stage? = null

    val initStage : Signal<Config?> = Signal()
    val disposeStage : Signal<Stage> = Signal()

    fun add(key : String, stage : Stage) {
        val s = stageMap[key]
        if (s == null) {
            stageMap[key] = stage
        } else {
            throw Exception("Stage with the same key already exist, key: ${key}")
        }

    }

    fun init() {
        // TODO: implement this if current stage doesn't exist
    }

    fun dispose() {
        if (currentStage != null) {
            disposeStage.dispatch(currentStage)
            currentStage!!.dispose()
        }
    }

    fun update() {
        if (currentStage != null) {
            currentStage!!.update()
        }
    }

    fun set(key : String) {
        val stage = stageMap[key]
        if (stage != null) {
            if (currentStage != null) {
                disposeStage.dispatch(currentStage)
                currentStage!!.dispose()
            }
            currentStage = stage
            initStage.dispatch(currentStage!!.configure())
            currentStage!!.create()
        }
    }

    fun get() : Stage {
        return currentStage!!
    }

    fun remove(key : String) {
        val stage = stageMap[key]
        if (stage != null) {
            stageMap.remove(key)
        }
    }
}