package com.disgraded.engine.manager

import com.badlogic.ashley.signals.Signal
import com.badlogic.gdx.Gdx
import com.badlogic.gdx.Input
import com.badlogic.gdx.InputProcessor
import com.badlogic.gdx.math.Vector2
import com.disgraded.engine.Engine
import java.util.*
import kotlin.collections.ArrayList



class InputManager(private val engine: Engine) : InputProcessor {

    class Touch(var position : Vector2, var pressure : Float, var previous : Vector2, val start : Vector2,
                val pointer : Int, val uid  : String = UUID.randomUUID().toString()) {
        fun isDragged() : Boolean {
            return position.x != previous.x && position.y != previous.y
        }
    }

    val onTouchDown : Signal<Touch> = Signal()
    val onTouch : Signal<Touch> = Signal()
    val onTouchUp : Signal<Touch> = Signal()
    val onTouchDragged : Signal<Touch> = Signal()

    val onMouseScroll : Signal<Int> = Signal()
    val onMouseMove : Signal<Vector2> = Signal()

    val onKeyTyped : Signal<String> = Signal()
    val onKeyDown : Signal<String> = Signal()
    val onKeyUp : Signal<String> = Signal()

    private var touchData : ArrayList<Touch?> = arrayListOf()

    fun init() {
        touchData = arrayListOf()
        for (i in 0 until engine.root.config.input.maxPointers) {
            touchData.add(null)
        }
        Gdx.input.inputProcessor = this
    }

    fun update() {
        for (i in 0 until engine.root.config.input.maxPointers) {
            if (Gdx.input.isTouched(i)) {
                isTouched(i)
            } else {
                notTouched(i)
            }
        }
    }

    fun dispose() {
        Gdx.input.inputProcessor = null
        onTouchUp.removeAllListeners()
        onTouch.removeAllListeners()
        onTouchDown.removeAllListeners()
        onTouchDragged.removeAllListeners()
        onKeyDown.removeAllListeners()
        onKeyTyped.removeAllListeners()
        onKeyUp.removeAllListeners()
        onMouseMove.removeAllListeners()
        onMouseScroll.removeAllListeners()
    }

    private fun isTouched(pointer: Int) {
        val screen = Vector2(Gdx.input.getX(pointer).toFloat(), Gdx.input.getY(pointer).toFloat())
        val pressure = Gdx.input.getPressure(pointer)
        val position = getPosition(screen)

        if (touchData[pointer] === null) {
            touchData[pointer] = Touch(position, pressure, position, position, pointer)
            onTouchDown.dispatch(touchData[pointer])
        } else {
            touchData[pointer]!!.previous = touchData[pointer]!!.position
            touchData[pointer]!!.pressure = pressure
            touchData[pointer]!!.position = position
            onTouch.dispatch(touchData[pointer])
            if (touchData[pointer]!!.isDragged()) {
                onTouchDragged.dispatch(touchData[pointer])
            }
        }
    }

    private fun notTouched(pointer: Int) {
        if (touchData[pointer] !== null) {
            val screen = Vector2(Gdx.input.getX(pointer).toFloat(), Gdx.input.getY(pointer).toFloat())
            val pressure = Gdx.input.getPressure(pointer)
            val position = getPosition(screen)

            touchData[pointer]!!.previous = touchData[pointer]!!.position
            touchData[pointer]!!.pressure = pressure
            touchData[pointer]!!.position = position

            if (touchData[pointer]!!.isDragged()) {
                onTouchDragged.dispatch(touchData[pointer])
            }

            onTouchUp.dispatch(touchData[pointer])

            touchData[pointer] = null
        }
    }

    private fun getPosition(screen: Vector2): Vector2 {
        val position = Vector2()
        val viewportSize = engine.root.config.graphics.viewportSize
        val windowSize = engine.windowSize
        position.x = (screen.x / windowSize.x * viewportSize.x) - (viewportSize.x / 2)
        position.y = ((screen.y / windowSize.y * viewportSize.y) - (viewportSize.y / 2)) * -1f
        return position
    }

    override fun touchDown(screenX: Int, screenY: Int, pointer: Int, button: Int): Boolean {
        return false
    }

    override fun touchDragged(screenX: Int, screenY: Int, pointer: Int): Boolean {
        return false
    }

    override fun touchUp(screenX: Int, screenY: Int, pointer: Int, button: Int): Boolean {
        return false
    }

    override fun mouseMoved(screenX: Int, screenY: Int): Boolean {
        onMouseMove.dispatch(getPosition(Vector2(screenX.toFloat(), screenY.toFloat())))
        return true
    }

    override fun scrolled(amount: Int): Boolean {
        onMouseScroll.dispatch(amount)
        return true
    }

    override fun keyTyped(character: Char): Boolean {
        onKeyTyped.dispatch(character.toString())
        return true
    }

    override fun keyUp(keycode: Int): Boolean {
        onKeyUp.dispatch(Input.Keys.toString(keycode))
        return true
    }

    override fun keyDown(keycode: Int): Boolean {
        onKeyDown.dispatch(Input.Keys.toString(keycode))
        return true
    }
}
