package com.disgraded.engine.manager

import com.disgraded.engine.Engine
import com.disgraded.engine.util.debug.Tracer
import java.lang.Exception

class DebugManager(private val engine: Engine) {

    private val tracerMap : HashMap<String, Tracer> = hashMapOf()

    fun init() {
        if (engine.root.config.debugMode) {
            createTracer("game", "Game loop")
            createTracer("stage", "Stage loop")
            createTracer("object", "Object loop")
            createTracer("render", "Rendering system")
            createTracer("physics", "Physics system")
            createTracer("input", "Input system")
            createTracer("task", "Task system")
        }
    }

    fun update() {

    }

    fun dispose() {

    }

    fun begin(name : String) {
        if (tracerMap[name] !== null && engine.root.config.debugMode) {
            tracerMap[name]!!.begin()
        }
    }

    fun end(name : String) {
        if (tracerMap[name] !== null && engine.root.config.debugMode) {
            tracerMap[name]!!.end()
        }
    }

    fun createTracer(name : String, description : String, records: Int = 720) {
        if (tracerMap.contains(name)) {
            throw Exception("Tracer with trace name $name already exist")
        } else {
            tracerMap[name] = Tracer(description, records)
        }
    }

    fun removeTracer(name : String) {
        if (tracerMap.contains(name)) {
            tracerMap.remove(name)
        } else {
            throw Exception("Tracer $name doesn't exist")
        }
    }

    fun printEngineStats() {
        if (engine.root.config.debugMode) {
            val gameLoop : Tracer = tracerMap["game"]!!
            gameLoop.calculate()
            println("======== DISGRADED ENGINE DEBUG STATISTICS ========")
            for (tracerPair in tracerMap.toList()) {
                val tracer = tracerPair.second
                if (tracer.calculate()) {
                    println(" * Type: ${tracer.description}")
                    print("Best time: ${tracer.bestStat} ms , Worst time: ${tracer.worstStat} ms")
                    println(" , Average time: ${tracer.average} ms")
                    print("Best: ${(tracer.bestStat * 100 / gameLoop.bestStat).toInt()}% , Worst: ${(tracer.worstStat * 100 / gameLoop.worstStat).toInt()}%")
                    println(" , Average ${(tracer.average * 100 / gameLoop.average).toInt()}%")
                    println("===================================================")

                } else {
                    println("Not enough data to calculate ${tracer.description} stats")
                    println("===================================================")
                }
            }
        }
    }
}