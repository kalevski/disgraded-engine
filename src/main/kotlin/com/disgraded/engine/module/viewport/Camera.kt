package com.disgraded.engine.module.viewport

import com.badlogic.gdx.graphics.OrthographicCamera
import com.badlogic.gdx.math.Vector2

class Camera(private val camera: OrthographicCamera, private val pixelRatio : Float) {

    fun getPosition() : Vector2 {
        val cameraPosition = camera.position
        return Vector2(cameraPosition.x * pixelRatio, cameraPosition.y * pixelRatio)
    }

    fun setPosition() {
        // TODO: implement this
    }
}