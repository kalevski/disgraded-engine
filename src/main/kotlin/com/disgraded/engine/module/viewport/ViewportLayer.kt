package com.disgraded.engine.module.viewport

import com.badlogic.gdx.graphics.OrthographicCamera
import com.badlogic.gdx.graphics.g2d.SpriteBatch
import com.badlogic.gdx.math.Matrix4
import com.badlogic.gdx.math.Vector2
import com.badlogic.gdx.utils.Scaling
import com.badlogic.gdx.utils.viewport.ScalingViewport
import com.badlogic.gdx.utils.viewport.Viewport
import com.disgraded.engine.Engine
import com.disgraded.engine.module.viewport.Camera
import com.disgraded.engine.util.Config

class ViewportLayer(private val engine: Engine, viewportType : Config.Graphics.ViewportType, viewportSize : Vector2) {

    private val pixelRatio = engine.root.config.graphics.pixelRatio

    private var viewport : Viewport
    private var spriteBatch : SpriteBatch = SpriteBatch()

    var camera : Camera

    init {
        viewport = ScalingViewport(getScaling(viewportType),viewportSize.x / pixelRatio, viewportSize.y / pixelRatio)
        camera = Camera(viewport.camera as OrthographicCamera, pixelRatio)
    }

    private fun getScaling(viewportType : Config.Graphics.ViewportType): Scaling {
        if (viewportType == Config.Graphics.ViewportType.FILL)  return Scaling.fill
        if (viewportType == Config.Graphics.ViewportType.FILL_X) return Scaling.fillX
        if (viewportType == Config.Graphics.ViewportType.FILL_Y) return Scaling.fillY
        if (viewportType == Config.Graphics.ViewportType.STRETCH) return Scaling.stretch
        if (viewportType == Config.Graphics.ViewportType.STRETCH_X) return Scaling.stretchX
        if (viewportType == Config.Graphics.ViewportType.STRETCH_Y) return Scaling.stretchY
        if (viewportType == Config.Graphics.ViewportType.FIT)  return Scaling.fit
        return Scaling.none
    }

    fun resize(windowSize : Vector2) {
        viewport.update(windowSize.x.toInt(), windowSize.y.toInt(), false)
    }

    fun update(deltaTime : Float) {
        viewport.update(engine.windowSize.x.toInt(), engine.windowSize.y.toInt())
        spriteBatch.projectionMatrix = viewport.camera.combined
    }

    fun dispose() {
        spriteBatch.dispose()
    }

    fun getProjectionMatrix(): Matrix4 {
        return viewport.camera.combined
    }

    fun getSpriteBatch(): SpriteBatch {
        return spriteBatch
    }
}