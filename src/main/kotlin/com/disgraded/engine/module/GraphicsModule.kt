package com.disgraded.engine.module

import com.badlogic.gdx.graphics.glutils.ShapeRenderer
import com.badlogic.gdx.math.Vector2
import com.badlogic.gdx.physics.box2d.Box2DDebugRenderer
import com.disgraded.engine.Engine
import com.disgraded.engine.module.viewport.ViewportLayer

class GraphicsModule(val engine : Engine) {

    lateinit var physicsDebugRenderer : Box2DDebugRenderer
    lateinit var inputDebugRenderer: ShapeRenderer
    lateinit var viewportLayer : ViewportLayer


    fun init() {
        physicsDebugRenderer = Box2DDebugRenderer()
        inputDebugRenderer = ShapeRenderer()
        viewportLayer = ViewportLayer(engine, engine.root.config.graphics.viewportType, engine.root.config.graphics.viewportSize)
    }

    fun resize(windowSize : Vector2) {
        viewportLayer.resize(windowSize)
    }

    fun update(deltaTime : Float) {
        viewportLayer.update(deltaTime)
    }

    fun dispose() {
        viewportLayer.dispose()
        physicsDebugRenderer.dispose()
        inputDebugRenderer.dispose()
    }
}