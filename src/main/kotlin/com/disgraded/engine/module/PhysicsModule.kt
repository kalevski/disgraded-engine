package com.disgraded.engine.module

import com.badlogic.gdx.physics.box2d.Box2D
import com.badlogic.gdx.physics.box2d.World
import com.disgraded.engine.Engine

class PhysicsModule(val engine: Engine) {

    lateinit var world : World

    init {
        Box2D.init()
    }

    fun init() {
        world = World(engine.root.config.physics.gravity, engine.root.config.physics.doSleep)
    }

    fun dispose() {
        world.dispose()
    }
}