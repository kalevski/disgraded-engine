package com.disgraded.engine.system

import com.badlogic.ashley.core.Family
import com.badlogic.gdx.Gdx
import com.badlogic.gdx.graphics.GL20
import com.badlogic.gdx.graphics.glutils.ShapeRenderer
import com.badlogic.gdx.math.Vector2
import com.disgraded.engine.adapter.Entity
import com.disgraded.engine.adapter.IteratingSystem
import com.disgraded.engine.component.InputComponent
import com.disgraded.engine.util.graphics.Color
import com.disgraded.engine.util.input.CircleShape
import com.disgraded.engine.util.input.EllipseShape
import com.disgraded.engine.util.input.InputShape
import com.disgraded.engine.util.input.RectangleShape

class DebugSystem : IteratingSystem(Family.all().get()) {

    override fun added() {

    }

    override fun update(deltaTime: Float) {
        if (engine.root.config.debugMode) {
            super.update(deltaTime)
        }
    }

    override fun filter(entityList: ArrayList<Entity>): ArrayList<Entity> {
        return entityList
    }

    override fun before(entityList: ArrayList<Entity>, deltaTime: Float) {
        val projectionMatrix = engine.graphics.viewportLayer.getProjectionMatrix()
        val inputDebugRenderer = engine.graphics.inputDebugRenderer
        val physicsDebugRenderer = engine.graphics.physicsDebugRenderer
        val world = engine.physics.world

        physicsDebugRenderer.render(world, projectionMatrix)

        inputDebugRenderer.projectionMatrix = projectionMatrix

        Gdx.gl.glEnable(GL20.GL_BLEND);
        Gdx.gl.glBlendFunc(GL20.GL_SRC_ALPHA, GL20.GL_ONE_MINUS_SRC_ALPHA);
    }

    override fun each(entity: Entity, deltaTime: Float) {
        val input = entity.getComponent(InputComponent::class.java)

        if (input !== null) {
            if (input.inputEnabled) {
                val inputDebugRenderer = engine.graphics.inputDebugRenderer
                inputDebugRenderer.begin(ShapeRenderer.ShapeType.Filled)
                if (input.isTouched) {
                    inputDebugRenderer.color = Color("#ffffff", .5f).getColor()
                } else {
                    inputDebugRenderer.color = Color("#000000", .5f).getColor()
                }
                renderShape(input)
                inputDebugRenderer.end()
            }
        }
    }

    override fun after(entityList: ArrayList<Entity>, deltaTime: Float) {
        Gdx.gl.glDisable(GL20.GL_BLEND);
    }

    override fun removed() {

    }

    private fun renderShape(input : InputComponent) {
        val pixelRatio = engine.root.config.graphics.pixelRatio
        val inputDebugRenderer = engine.graphics.inputDebugRenderer
        if (input.getShape().getType() == InputShape.Type.RECTANGLE) {
            val shape = input.getShape() as RectangleShape
            var position = Vector2(shape.getPosition().x / pixelRatio, shape.getPosition().y / pixelRatio)
            var size = Vector2(shape.size.x / pixelRatio, shape.size.y / pixelRatio)
            var offset = Vector2(shape.offset.x / pixelRatio, shape.offset.y / pixelRatio)
            position.x = position.x - size.x / 2 + offset.x
            position.y = position.y - size.y / 2 + offset.y
            inputDebugRenderer.rect(position.x, position.y, size.x, size.y)
        }
        if (input.getShape().getType() == InputShape.Type.CIRCLE) {
            val shape = input.getShape() as CircleShape
            var position = Vector2(shape.getPosition().x / pixelRatio, shape.getPosition().y / pixelRatio)
            var radius = shape.radius / pixelRatio
            var offset = Vector2(shape.offset.x / pixelRatio, shape.offset.y / pixelRatio)
            position.x += offset.x
            position.y += offset.y
            inputDebugRenderer.circle(position.x, position.y, radius)
        }
        if (input.getShape().getType() == InputShape.Type.ELLIPSE) {
            val shape = input.getShape() as EllipseShape
            var position = Vector2(shape.getPosition().x / pixelRatio, shape.getPosition().y / pixelRatio)
            var size = Vector2(shape.size.x / pixelRatio, shape.size.y / pixelRatio)
            var offset = Vector2(shape.offset.x / pixelRatio, shape.offset.y / pixelRatio)
            position.x = position.x + offset.x - size.x / 2
            position.y = position.y + offset.y - size.y / 2
            inputDebugRenderer.ellipse(position.x, position.y, size.x, size.y)
        }
    }
}