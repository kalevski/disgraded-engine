package com.disgraded.engine.system

import com.badlogic.ashley.core.Family
import com.badlogic.gdx.math.Vector2
import com.disgraded.engine.adapter.Entity
import com.disgraded.engine.adapter.IteratingSystem
import com.disgraded.engine.component.PhysicsComponent
import com.disgraded.engine.component.TransformComponent

class PhysicsSystem : IteratingSystem(Family.all(
            TransformComponent::class.java,
            PhysicsComponent::class.java
    ).get()) {

    private var accumulator : Float = 0f

    override fun added() {

    }

    override fun filter(entityList: ArrayList<Entity>): ArrayList<Entity> {
        return entityList
    }

    override fun before(entityList: ArrayList<Entity>, deltaTime: Float) {
        engine.root.debug.begin("physics")
        val world = engine.physics.world

        val frameTime = Math.min(deltaTime, 0.25f)
        val worldTimestamp = 1 / 300f * engine.root.config.physics.worldSpeed
        val velocityIterations = engine.root.config.physics.velocityIterations
        val positionIterations = engine.root.config.physics.positionIterations

        accumulator += frameTime
        while(accumulator >= worldTimestamp) {
            world.step(worldTimestamp, velocityIterations, positionIterations)
            accumulator -= worldTimestamp
        }
    }

    override fun each(entity: Entity, deltaTime: Float) {
        val transform = entity.getComponent(TransformComponent::class.java)
        val physics = entity.getComponent(PhysicsComponent::class.java)

        if (physics !== null) {
            syncPhysics(entity, transform, physics)
        }

        val pixelRatio : Float = engine.root.config.graphics.pixelRatio

        val positionX : Float = physics.getBody().position.x * pixelRatio
        val positionY : Float = physics.getBody().position.y * pixelRatio
        var rotation : Float = (physics.getBody().angle * (180f / Math.PI)).toFloat()

        var deviation = Math.abs(Math.floor((rotation / 360f).toDouble()))
        if (rotation > 0f && deviation > 0) {
            rotation -= 360f * deviation.toFloat()
        }
        else if (rotation < 0f && deviation > 0) {
            rotation += 360f * deviation.toFloat()
        }

        transform.position = Vector2(positionX, positionY)
        transform.rotation = rotation
    }

    override fun after(entityList: ArrayList<Entity>, deltaTime: Float) {
        engine.root.debug.end("physics")
    }

    override fun removed() {

    }

    private fun syncPhysics(entity: Entity, transform : TransformComponent, physics : PhysicsComponent) {
        if (!physics.initialized) {
            physics.setTransform(transform.position, transform.rotation)
            physics.initialized = true
        }
    }
}