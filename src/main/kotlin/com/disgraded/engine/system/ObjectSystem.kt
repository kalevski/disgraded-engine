package com.disgraded.engine.system

import com.badlogic.ashley.core.Family
import com.disgraded.engine.adapter.Entity
import com.disgraded.engine.adapter.IteratingSystem
import com.disgraded.engine.component.CoreComponent
import com.disgraded.engine.component.PhysicsComponent
import com.disgraded.engine.component.TransformComponent


class ObjectSystem : IteratingSystem(Family.all(
            CoreComponent::class.java
    ).get()) {

    override fun added() {

    }

    override fun filter(entityList: ArrayList<Entity>): ArrayList<Entity> {
        return entityList
    }

    override fun before(entityList: ArrayList<Entity>, deltaTime: Float) {
        engine.root.debug.begin("object")
    }

    override fun each(entity: Entity, deltaTime: Float) {
        entity.render(deltaTime)
    }

    override fun after(entityList: ArrayList<Entity>, deltaTime: Float) {
        engine.root.debug.end("object")
    }

    override fun removed() {

    }
}