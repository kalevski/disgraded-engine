package com.disgraded.engine.system

import com.badlogic.ashley.core.Family
import com.badlogic.gdx.graphics.g2d.Sprite
import com.badlogic.gdx.graphics.g2d.SpriteBatch
import com.badlogic.gdx.graphics.g2d.TextureRegion
import com.disgraded.engine.adapter.Entity
import com.disgraded.engine.adapter.IteratingSystem
import com.disgraded.engine.component.GraphicsComponent
import com.disgraded.engine.component.TransformComponent
import java.awt.Color

class RenderingSystem : IteratingSystem(Family.all(
            TransformComponent::class.java,
            GraphicsComponent::class.java
    ).get()) {

    override fun added() {

    }

    override fun filter(entityList: ArrayList<Entity>): ArrayList<Entity> {
        entityList.sortBy { it.getComponent(GraphicsComponent::class.java).getRenderOrder() }
        return entityList
    }

    override fun before(entityList: ArrayList<Entity>, deltaTime: Float) {
        engine.root.debug.begin("render")
        val spriteBatch : SpriteBatch = engine.graphics.viewportLayer.getSpriteBatch()
        spriteBatch.begin()
    }

    override fun each(entity: Entity, deltaTime: Float) {
        val spriteBatch : SpriteBatch = engine.graphics.viewportLayer.getSpriteBatch()
        val pixelRatio : Float = engine.root.config.graphics.pixelRatio

        val transform = entity.getComponent(TransformComponent::class.java)
        val graphics = entity.getComponent(GraphicsComponent::class.java)

        adaptComponents(transform, graphics)

        if (graphics.visible) {
            graphics.getRenderingUnits().sortBy { unit -> unit.zOrder }
            for (renderingUnit in graphics.getRenderingUnits()) {
                if (renderingUnit.textureRegion == null) continue
                if (!renderingUnit.visible) continue

                val textureRegion : TextureRegion = renderingUnit.textureRegion!!

                var flippingX = graphics.flipX != textureRegion.isFlipX
                var flippingY = graphics.flipY != textureRegion.isFlipY
                if (renderingUnit.flipX) flippingX = !flippingX
                if (renderingUnit.flipY) flippingY = !flippingY
                textureRegion.flip(flippingX, flippingY)

                val width = textureRegion.regionWidth / pixelRatio
                val height = textureRegion.regionHeight / pixelRatio
                val positionX = (transform.position.x / pixelRatio) + (graphics.anchor.x * width * -1f) + renderingUnit.offset.x / pixelRatio
                val positionY = (transform.position.y / pixelRatio) + (graphics.anchor.y * width * -1f) + renderingUnit.offset.y / pixelRatio
                val originX = width * graphics.anchor.x + graphics.origin.x / pixelRatio - renderingUnit.offset.x / pixelRatio
                val originY = height * graphics.anchor.y + graphics.origin.y / pixelRatio - renderingUnit.offset.y / pixelRatio
                val scaleX = graphics.scale.x * renderingUnit.scale.x
                val scaleY = graphics.scale.y * renderingUnit.scale.y
                val rotation = transform.rotation

                val originalColor = spriteBatch.color.cpy()
                var color = originalColor.cpy()

                if (renderingUnit.tint !== null) {
                    color = renderingUnit.tint!!.getColor()
                } else if (renderingUnit.tint === null && graphics.tint !== null) {
                    color = graphics.tint!!.getColor()
                }
                color.a = graphics.alpha * renderingUnit.alpha
                spriteBatch.color = color
                spriteBatch.draw(textureRegion, positionX, positionY, originX, originY, width, height,
                        scaleX, scaleY, rotation)
                spriteBatch.color = originalColor
            }
        }
    }

    override fun after(entityList: ArrayList<Entity>, deltaTime: Float) {
        val spriteBatch : SpriteBatch = engine.graphics.viewportLayer.getSpriteBatch()
        spriteBatch.end()
        engine.root.debug.end("render")
    }

    override fun removed() {

    }

    private fun adaptComponents(transform : TransformComponent, graphics : GraphicsComponent) {
        // anchor range limiter
        if (graphics.anchor.x > 1f) graphics.anchor.x = 1f
        else if (graphics.anchor.x < 0f) graphics.anchor.x = 0f
        if (graphics.anchor.y > 1f) graphics.anchor.y = 1f
        else if (graphics.anchor.y < 0f) graphics.anchor.y = 0f

        // rotation limiter
        val deviation = Math.abs(Math.floor(transform.rotation.toDouble() / 360f).toFloat())
        if (transform.rotation > 0f && deviation > 0) transform.rotation -= 360f * deviation
        else if (transform.rotation < 0f && deviation > 0) transform.rotation += 360f * deviation
    }
}