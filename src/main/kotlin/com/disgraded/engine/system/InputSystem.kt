package com.disgraded.engine.system

import com.badlogic.ashley.core.Family
import com.badlogic.gdx.math.Vector2
import com.disgraded.engine.adapter.Entity
import com.disgraded.engine.adapter.IteratingSystem
import com.disgraded.engine.component.GraphicsComponent
import com.disgraded.engine.component.InputComponent
import com.disgraded.engine.component.TransformComponent
import com.disgraded.engine.manager.InputManager
import kotlin.collections.ArrayList

class InputSystem : IteratingSystem(Family.all(
            InputComponent::class.java
    ).get()) {

    override fun added() {
        engine.root.input.onTouchDown.add { _, touch -> touch(touch, true, false, false) }
        engine.root.input.onTouch.add { _, touch -> touch(touch, false, true, false) }
        engine.root.input.onTouchUp.add { _, touch -> touch(touch, false, false, true) }
    }

    override fun filter(entityList: ArrayList<Entity>): ArrayList<Entity> {
        return (entityList.filter { entity -> entity.getComponent(InputComponent::class.java).inputEnabled } as ArrayList<Entity>)
    }

    override fun before(entityList: ArrayList<Entity>, deltaTime: Float) {
        engine.root.debug.begin("input")
    }

    override fun each(entity: Entity, deltaTime: Float) {
        val input = entity.getComponent(InputComponent::class.java)
        val transform = entity.getComponent(TransformComponent::class.java)
        input.getShape().update(transform.position)
        input.isTouched = false
    }

    override fun after(entityList: ArrayList<Entity>, deltaTime: Float) {
        engine.root.debug.end("input")
    }

    override fun removed() {

    }

    private fun touch(touch: InputManager.Touch, touchDown : Boolean, touched : Boolean, touchUp : Boolean) {
        val entityList = getList()

        var touchedEntities : ArrayList<Entity> = arrayListOf()
        var touchedPriority : Int = -1

        for (entity in entityList) {
            val input = entity.getComponent(InputComponent::class.java)
            if (!input.inputEnabled) continue
            val position = Vector2(touch.position.x , touch.position.y)
            position.add(engine.graphics.viewportLayer.camera.getPosition())
            if (!input.getShape().contains(position)) continue
            if (input.priority > touchedPriority) {
                touchedEntities.clear()
                touchedPriority = input.priority
                touchedEntities.add(entity)
            } else if (input.priority == touchedPriority) {
                touchedEntities.add(entity)
            }
        }

        touchedEntities.sortBy { entity -> graphicsComparator(entity) }

        if (touchedEntities.isNotEmpty()) {
            val entity = touchedEntities.last()
            if (touchDown) {
                entity.getComponent(InputComponent::class.java).onTouchDown.dispatch(touch)
                entity.getComponent(InputComponent::class.java).isTouched = true
            } else if (touched) {
                entity.getComponent(InputComponent::class.java).onTouch.dispatch(touch)
                entity.getComponent(InputComponent::class.java).isTouched = true
            } else if (touchUp) {
                entity.getComponent(InputComponent::class.java).onTouchUp.dispatch(touch)
                entity.getComponent(InputComponent::class.java).isTouched = true
            }
        }
    }

    private fun graphicsComparator(entity : Entity) : Long {
        val graphics = entity.getComponent(GraphicsComponent::class.java)
        if (graphics !== null) {
            return graphics.getRenderOrder()
        } else {
            return 0
        }
    }
}