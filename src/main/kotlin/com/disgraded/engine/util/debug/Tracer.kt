package com.disgraded.engine.util.debug

import java.lang.Exception
import java.util.*

class Tracer(val description : String, private val records : Int) {
    private var start : Long = 0
    private var stop : Long = 0
    private val recordList : ArrayList<Float> = arrayListOf()

    val statList : ArrayList<Float> = arrayListOf()
    var bestStat : Float = 0f
    var worstStat : Float = 0f
    var average : Float = 0f

    fun begin() {
        start = Date().time
    }

    fun end() {
        stop = Date().time
        val diff = (stop - start).toFloat()
        recordList.add(diff)
        if (recordList.size >= records) {
            var sum : Float = 0f
            for (record in recordList) {
                sum += record
            }
            sum /= records
            statList.add(sum)
            recordList.clear()
        }
    }

    fun calculate() : Boolean {
        try {
            bestStat = statList.first()
            worstStat = statList.first()
            var sum : Float = 0f
            for (stat in statList) {
                if (stat > worstStat) {
                    worstStat = stat
                }
                if (stat < bestStat) {
                    bestStat = stat
                }
                sum += stat
            }
            average = sum / statList.size
            return true
        } catch (e : Exception) {
            return false
        }
    }
}