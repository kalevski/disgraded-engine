package com.disgraded.engine.util.physics

import com.badlogic.gdx.physics.box2d.BodyDef

class Physics2DDef {

    var bodyType : BodyDef.BodyType = BodyDef.BodyType.StaticBody
    var angle : Float = 0f
    var angularVelocity : Float = 0f
    var linearDamping : Float = 0f
    var angularDamping : Float = 0f
    var allowSleep : Boolean = true
    var awake : Boolean = true
    var fixedRotation : Boolean = false
    var bullet : Boolean = false
    var active : Boolean = true
    var gravityScale : Float = 1f
}