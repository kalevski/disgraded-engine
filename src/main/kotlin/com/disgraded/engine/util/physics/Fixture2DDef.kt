package com.disgraded.engine.util.physics

import com.badlogic.gdx.math.Vector2
import com.badlogic.gdx.physics.box2d.*
import com.disgraded.engine.Engine
import java.lang.Exception
import java.util.*
import kotlin.collections.ArrayList

class Fixture2DDef {

    private val engine : Engine = Engine.getInstance()

    private val signature : String = UUID.randomUUID().toString()

    private var fixtureDef : FixtureDef = FixtureDef()

    public enum class ShapeType {
        CIRCLE, BOX, EDGE, POLYGON, CHAIN
    }

    /** The friction coefficient, usually in the range [0,1]. **/
    var friction : Float = 0.2f

    /** The restitution (elasticity) usually in the range [0,1]. **/
    var restitution : Float = 0f

    /** The density, usually in kg/m^2. **/
    var density : Float = 0f

    /** A sensor shape collects contact information but never generates a collision response. */
    var isSensor : Boolean = false

    var shapeType : ShapeType =
            ShapeType.CIRCLE

    var radius : Float = 0f
    var boxSize : Vector2 = Vector2(0f, 0f)
    var vertexList : ArrayList<Vector2> = ArrayList()

    fun generateFixture() {
        val pixelRatio = engine.root.config.graphics.pixelRatio
        fixtureDef = FixtureDef()
        fixtureDef.density = density
        fixtureDef.friction = friction
        fixtureDef.restitution = restitution
        fixtureDef.isSensor = isSensor

        if (shapeType == ShapeType.CIRCLE) {
            val shape = CircleShape()
            if (radius == 0f) {
                throw Exception("You must set radius for shape type CIRCLE")
            }
            shape.radius = radius / pixelRatio
            fixtureDef.shape = shape
        }

        if (shapeType == ShapeType.BOX) {
            val shape = PolygonShape()
            if (boxSize.x == 0f || boxSize.y == 0f) {
                throw Exception("You must set box size for shape type BOX")
            }
            shape.setAsBox(boxSize.x / pixelRatio / 2, boxSize.y / pixelRatio / 2)
            fixtureDef.shape = shape
        }

        if (shapeType == ShapeType.EDGE) {
            val shape = EdgeShape()
            var vertexList : ArrayList<Vector2> = ArrayList()
            for (vertex in this.vertexList) {
                vertexList.add(Vector2(vertex.x / pixelRatio, vertex.y / pixelRatio))
            }
            if (vertexList.size != 2) {
                throw Exception("Vertex list must contain 2 vertices for shape type EDGE")
            }
            shape.set(vertexList.get(0), vertexList.get(1))
            fixtureDef.shape = shape
        }

        if (shapeType == ShapeType.POLYGON) {
            val shape = PolygonShape()
            var vertexList : ArrayList<Vector2> = ArrayList()
            for (vertex in this.vertexList) {
                vertexList.add(Vector2(vertex.x / pixelRatio, vertex.y / pixelRatio))
            }
            if (vertexList.size < 3) {
                throw Exception("Vertex list must contain at least 3 vertices for shape type POLYGON")
            }
            var array = arrayOf<Vector2>()
            shape.set(array.plus(vertexList))
            fixtureDef.shape = shape
        }

        if (shapeType == ShapeType.CHAIN) {
            val shape = ChainShape()
            var vertexList : ArrayList<Vector2> = ArrayList()
            for (vertex in this.vertexList) {
                vertexList.add(Vector2(vertex.x / pixelRatio, vertex.y / pixelRatio))
            }
            if (vertexList.size < 3) {
                throw Exception("Vertex list must contain at least 3 vertices for shape type CHAIN")
            }
            for (vertex in vertexList) {
                shape.setNextVertex(vertex)
            }
            fixtureDef.shape = shape
        }
    }

    fun getFixtureDef() : FixtureDef {
        return fixtureDef
    }

    fun getSignature() : String {
        return signature
    }

    fun getFilter(): Filter {
        return Filter()
    }
}