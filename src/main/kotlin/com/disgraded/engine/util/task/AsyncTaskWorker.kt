package com.disgraded.engine.util.task

import com.disgraded.engine.Engine
import java.lang.Exception
import java.util.concurrent.BlockingDeque

class AsyncTaskWorker(private val asyncQueue : BlockingDeque<TaskInvoker>, private val engine: Engine) : TaskRunnable() {

    private var attempt : Int = 0
    private val maxAttempts : Int = 5

    override fun onStart() {

    }

    override fun getTaskInvoker(): TaskInvoker? {
        var taskInvoker : TaskInvoker?
        try {
            taskInvoker = asyncQueue.take()
        } catch (e : Exception) {
            error(e)
        }
        if (taskInvoker !== null) {
            try {
                if (taskInvoker.task.canRepeat(taskInvoker.data)) {
                    asyncQueue.put(taskInvoker)
                }
            } catch (e: Exception) {
                error(e)
            }
        }
        return taskInvoker
    }

    override fun canExecute(taskInvoker : TaskInvoker): Boolean {
        return taskInvoker.task.canExecute(taskInvoker.data)
    }

    override fun execute(taskInvoker: TaskInvoker) : Runnable? {
        taskInvoker.task.execute(taskInvoker.data)
        return Runnable {
            run {
                taskInvoker.task.synchronize(engine.root)
            }
        }
    }

    override fun sleep() : Long {
        var sleepingTime = 100L
        attempt++
        if (attempt >= maxAttempts) {
            sleepingTime = 1000L
            attempt = 0
        }
        return sleepingTime
    }

    override fun onStop() {

    }
}