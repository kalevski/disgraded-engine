package com.disgraded.engine.util.task

import com.disgraded.engine.Root

interface Task {
    fun canExecute(data: Any?) : Boolean
    fun canRepeat(data: Any?) : Boolean
    fun execute(data : Any?)
    fun synchronize(root : Root)
}