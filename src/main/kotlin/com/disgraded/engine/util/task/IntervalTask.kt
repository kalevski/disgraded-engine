package com.disgraded.engine.util.task

abstract class IntervalTask(private val ms : Long) : Task {

    private var repeat : Boolean = true
    private var timeAccumulator : Long = 0
    private var latestTime : Long = 0

    override fun canExecute(data: Any?): Boolean {
        val currentTime : Long = System.currentTimeMillis()
        if (latestTime == 0L) {
            latestTime = currentTime
            return true
        } else {
            timeAccumulator += currentTime - latestTime
            latestTime = currentTime
            if (timeAccumulator >= ms) {
                timeAccumulator -= ms * (timeAccumulator / ms)
                return true
            } else {
                return false
            }
        }
    }

    override fun canRepeat(data: Any?): Boolean {
        return repeat
    }

    fun stop() {
        repeat = false
    }
}