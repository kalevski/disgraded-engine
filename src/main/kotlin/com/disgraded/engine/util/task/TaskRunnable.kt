package com.disgraded.engine.util.task

import com.badlogic.gdx.Gdx
import java.lang.Exception

abstract class TaskRunnable : Runnable {

    private var running = true

    fun stop() {
        running = false
    }

    override fun run() {
        onStart()
        while(running) {
            val taskInvoker : TaskInvoker? = getTaskInvoker()
            if (taskInvoker == null) {
                val sleepTime : Long = sleep()
                if (sleepTime != 0L) {
                    Thread.sleep(sleepTime)
                }
            } else {
                if (!canExecute(taskInvoker)) {
                    continue
                }
                val runnable = execute(taskInvoker)
                if (runnable !== null) {
                    Gdx.app.postRunnable(runnable)
                }
            }
        }
        onStop()
    }

    abstract fun onStart()

    abstract fun getTaskInvoker() : TaskInvoker?

    abstract fun canExecute(taskInvoker : TaskInvoker) : Boolean

    abstract fun execute(taskInvoker: TaskInvoker) : Runnable?

    abstract fun sleep() : Long

    abstract  fun onStop()
}