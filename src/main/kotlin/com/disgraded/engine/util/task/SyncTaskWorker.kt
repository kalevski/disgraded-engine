package com.disgraded.engine.util.task

import com.disgraded.engine.Engine
import java.lang.Exception
import java.util.*
import kotlin.collections.ArrayList

class SyncTaskWorker(private val syncQueue : Queue<TaskInvoker>, private val engine: Engine) {

    init {

    }

    fun update() {
        val taskInvokerList : ArrayList<TaskInvoker> = arrayListOf()
        while(!syncQueue.isEmpty()) {
            val taskInvoker = syncQueue.poll()
            if (taskInvoker.task.canRepeat(taskInvoker.data)) {
                taskInvokerList.add(taskInvoker)
            }
            if (taskInvoker.task.canExecute(taskInvoker.data)) {
                taskInvoker.task.execute(taskInvoker.data)
                taskInvoker.task.synchronize(engine.root)
            }
        }
        for (taskInvoker in taskInvokerList) {
            syncQueue.add(taskInvoker)
        }
    }

    fun dispose() {

    }
}