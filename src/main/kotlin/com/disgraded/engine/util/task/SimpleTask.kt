package com.disgraded.engine.util.task

abstract class SimpleTask : Task {
    override fun canExecute(data: Any?): Boolean {
        return true
    }

    override fun canRepeat(data: Any?): Boolean {
        return false
    }
}