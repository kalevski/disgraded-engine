package com.disgraded.engine.util

import com.badlogic.gdx.math.Vector2

class Config {

    class Graphics {
        enum class ViewportType { FIT, FILL, FILL_X, FILL_Y, STRETCH, STRETCH_X, STRETCH_Y }

        var pixelRatio : Float = 32f
        var viewportType = ViewportType.FIT
        var viewportSize = Vector2(1920f, 1080f)
    }

    class Physics {
        var gravity : Vector2 = Vector2(0f, 0f)
        var doSleep : Boolean = true
        var velocityIterations : Int = 6
        var positionIterations : Int = 2
        var worldSpeed = 1f
    }

    class Input {
        var maxPointers: Int = 5
    }

    var debugMode = false

    var graphics : Graphics = Graphics()
    var physics : Physics = Physics()
    var input : Input = Input()
}