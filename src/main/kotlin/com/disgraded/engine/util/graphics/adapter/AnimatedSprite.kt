package com.disgraded.engine.util.graphics.adapter

import com.badlogic.ashley.signals.Signal
import com.badlogic.gdx.Gdx
import com.badlogic.gdx.graphics.Texture
import com.badlogic.gdx.graphics.g2d.TextureRegion
import com.disgraded.engine.util.graphics.GraphicAdapter
import com.disgraded.engine.util.graphics.RenderingUnit
import java.lang.Exception

class AnimatedSprite : GraphicAdapter() {

    enum class Status { PLAY, REVERSE_PLAY, PAUSE, STOP }

    class Animation {
        var frames : ArrayList<RenderingUnit> = arrayListOf()
        var speed = 1f
        var loop = false
    }

    private val unitList : ArrayList<RenderingUnit> = arrayListOf(RenderingUnit())

    val onLoop : Signal<Boolean> = Signal()
    val onPause : Signal<Boolean> = Signal()
    val onStop : Signal<Boolean> = Signal()

    private var currentAnimation : Animation? = null
    private var status : Status = Status.STOP
    private var animationMap : HashMap<String, Animation> = hashMapOf()
    private var accumulator : Float = 0f

    fun add(key : String, texture: Texture, width: Int, height: Int, speed : Float = 1f, loop: Boolean = true, frameNo: Int = 0) {
        val animation = Animation()
        animation.frames = splitFrames(texture, width, height, frameNo)
        animation.speed = speed
        animation.loop = loop
        add(key, animation)
    }

    fun add(key : String, animation : Animation) {
        if (animationMap[key] !== null) {
            throw Exception("Key $key is already taken from other animation")
        }
        animationMap[key] = animation
    }

    fun set(key : String, resetAccumulator : Boolean = true) {
        val animation = animationMap[key]
        if (animation === null) {
            throw Exception("Animation with key [$key] doesn't exist")
        }
        currentAnimation = animation
        if (resetAccumulator) {
            accumulator = 0f
        }
    }

    fun play(reverse: Boolean = false, resetAccumulator: Boolean = false) {
        if (reverse && status != Status.REVERSE_PLAY) {
            status = Status.REVERSE_PLAY
            if (resetAccumulator) {
                accumulator = 0f
            }
        } else if (!reverse && status != Status.PLAY) {
            status = Status.PLAY
            if (resetAccumulator) {
                accumulator = 0f
            }
        }
    }

    fun pause() {
        status = Status.PAUSE
        onPause.dispatch(true)
    }

    fun stop() {
        status = Status.STOP
        accumulator = 0f
        onStop.dispatch(true)
    }

    fun getStatus() : Status {
        return status
    }

    fun getCurrentAnimation() : Animation? {
        return currentAnimation
    }

    fun getAnimation(key : String) : Animation? {
        return animationMap[key]
    }

    fun getAnimations(): List<Pair<String, Animation>> {
        return animationMap.toList()
    }

    private fun splitFrames(texture: Texture, width: Int, height: Int, frameNo : Int = 0): ArrayList<RenderingUnit> {
        val textureMatrix : Array<out Array<TextureRegion>> = TextureRegion.split(texture, texture.width / width, texture.height / height)
        val textures : ArrayList<RenderingUnit> = arrayListOf()
        for (textureRegionList in textureMatrix) {
            for (textureRegion in textureRegionList) {
                if (frameNo > 0 && textures.size == frameNo) {
                    break
                }
                val unit = RenderingUnit()
                unit.textureRegion = textureRegion
                textures.add(unit)
            }
        }
        return textures
    }

    override fun getRenderingUnits(): ArrayList<RenderingUnit> {
        incrementAccumulator()
        val currentFrameIndex = getCurrentFrameIndex()
        try {
            unitList[0] = currentAnimation!!.frames.get(currentFrameIndex)
        } catch (e : Exception) {
            error(e)
        }
        return unitList
    }

    private fun getCurrentFrameIndex(): Int {
        if (currentAnimation === null) {
            return 0
        }

        var index = Math.floor(accumulator.toDouble()).toInt()
        if (index < 0) {
            index += currentAnimation!!.frames.size
        }
        if (index >= currentAnimation!!.frames.size) {
            index -= currentAnimation!!.frames.size * Math.floor(index.toDouble() / currentAnimation!!.frames.size).toInt()
        }

        if (index >= 14) {
            error(currentAnimation!!.frames.size)
        }
        return index
    }

    private fun incrementAccumulator() {
        val deltaTime = Gdx.graphics.deltaTime
        if (status === Status.PLAY) {
            accumulator += deltaTime * currentAnimation!!.speed * 24
        }
        if (status === Status.REVERSE_PLAY) {
            accumulator -= deltaTime * currentAnimation!!.speed * 24
        }

        if (Math.abs(accumulator) >= currentAnimation!!.frames.size && accumulator > 0) {
            accumulator -= currentAnimation!!.frames.size
            if (!currentAnimation!!.loop) {
                stop()
            } else {
                onLoop.dispatch(true)
            }
        } else if (Math.abs(accumulator) >= currentAnimation!!.frames.size && accumulator < 0) {
            accumulator += currentAnimation!!.frames.size
            if (!currentAnimation!!.loop) {
                stop()
            } else {
                onLoop.dispatch(true)
            }
        }
    }
}