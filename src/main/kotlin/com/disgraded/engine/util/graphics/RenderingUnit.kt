package com.disgraded.engine.util.graphics

import com.badlogic.gdx.graphics.g2d.TextureRegion
import com.badlogic.gdx.math.Vector2

open class RenderingUnit {
    var textureRegion : TextureRegion? = null

    var offset : Vector2 = Vector2(0f, 0f)
    var scale : Vector2 = Vector2(1f, 1f)
    var flipX : Boolean = false
    var flipY : Boolean = false
    var zOrder : Int = 0
    var alpha : Float = 1f
    var tint : Color? = null
    var visible = true
}