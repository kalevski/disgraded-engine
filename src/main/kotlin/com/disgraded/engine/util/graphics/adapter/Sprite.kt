package com.disgraded.engine.util.graphics.adapter

import com.badlogic.gdx.graphics.Texture
import com.badlogic.gdx.graphics.g2d.TextureRegion
import com.disgraded.engine.util.graphics.GraphicAdapter
import com.disgraded.engine.util.graphics.RenderingUnit

class Sprite() : GraphicAdapter() {

    private val renderingUnit: RenderingUnit = RenderingUnit()
    private val unitList : ArrayList<RenderingUnit> = arrayListOf(renderingUnit)

    fun setTexture(texture: Texture) {
        renderingUnit.textureRegion = TextureRegion(texture, texture.width, texture.height)
    }

    fun setTexture(texture: Texture, x : Int, y : Int, width : Int, height: Int) {
        renderingUnit.textureRegion = TextureRegion(texture, x, y, width, height)
    }

    fun setTexture(textureRegion: TextureRegion) {
        renderingUnit.textureRegion = textureRegion
    }

    fun getRenderingUnit() : RenderingUnit {
        return renderingUnit
    }

    override fun getRenderingUnits(): ArrayList<RenderingUnit> {
        return unitList
    }
}