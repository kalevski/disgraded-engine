package com.disgraded.engine.util.graphics.adapter

import com.badlogic.gdx.graphics.Pixmap
import com.badlogic.gdx.graphics.Texture
import com.badlogic.gdx.graphics.g2d.TextureRegion
import com.disgraded.engine.util.graphics.Color
import com.disgraded.engine.util.graphics.GraphicAdapter
import com.disgraded.engine.util.graphics.RenderingUnit
import java.util.ArrayList

class CircleShape(radius : Float, color : Color) : GraphicAdapter() {

    private val renderingUnit: RenderingUnit = RenderingUnit()
    private val unitList : ArrayList<RenderingUnit> = arrayListOf(renderingUnit)

    init {
        val map = Pixmap((radius * 2).toInt(), (radius * 2).toInt(), Pixmap.Format.RGBA4444)
        map.setColor(color.getColor())
        map.fillCircle(radius.toInt(), radius.toInt(), radius.toInt())
        renderingUnit.textureRegion = TextureRegion(Texture(map))
        map.dispose()
    }

    fun getRenderingUnit() : RenderingUnit {
        return renderingUnit
    }

    override fun getRenderingUnits(): ArrayList<RenderingUnit> {
        return unitList
    }
}