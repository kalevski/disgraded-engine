package com.disgraded.engine.util.graphics

import com.badlogic.gdx.graphics.Color
import java.lang.Exception

class Color {

    private var r : Float = 0f
    private var g : Float = 0f
    private var b : Float = 0f
    private var a : Float = 1f

    constructor(r : Float, g : Float, b: Float, alpha : Float = 1f) {
        this.r = r
        this.g = g
        this.b = b
        this.a = alpha
    }

    constructor(hexColor : String, alpha : Float = 1f) {
        setColor(hexColor, alpha)
    }

    fun setColor(hexColor: String, alpha: Float) {
        if (hexColor.length != 7 || hexColor.first() != '#') {
            throw Exception("Hex code $hexColor isn't valid hex color code")
        }
        r = Integer.valueOf(hexColor.substring(1, 3), 16).toFloat() / 255
        g = Integer.valueOf(hexColor.substring(3, 5), 16).toFloat() / 255
        b = Integer.valueOf(hexColor.substring(5, 7), 16).toFloat() / 255
        a = alpha
    }

    fun setColor(r : Float, g : Float, b: Float, alpha : Float = 1f) {
        this.r = r
        this.g = g
        this.b = b
        this.a = alpha
    }

    fun setAlpha(alpha : Float) {
        a = alpha
    }

    fun getColor(): Color {
        return Color(r, g, b, a)
    }
}