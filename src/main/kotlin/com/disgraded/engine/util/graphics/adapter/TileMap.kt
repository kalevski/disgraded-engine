package com.disgraded.engine.util.graphics.adapter

import com.badlogic.gdx.graphics.Texture
import com.badlogic.gdx.graphics.g2d.TextureRegion
import com.badlogic.gdx.math.Vector2
import com.disgraded.engine.util.graphics.GraphicAdapter
import com.disgraded.engine.util.graphics.RenderingUnit
import java.util.ArrayList

class TileMap : GraphicAdapter() {

    private val tiles : ArrayList<RenderingUnit> = arrayListOf()
    private val mappedTiles : HashMap<String, TextureRegion> = hashMapOf()

    fun createTile(key : String, texture: Texture, x : Int, y : Int, width : Int, height: Int) {
        val textureRegion = TextureRegion(texture, x, y, width, height)
        createTile(key, textureRegion)
    }

    fun createTile(key : String, textureRegion: TextureRegion) {
        mappedTiles[key] = textureRegion
    }

    fun addTile(key : String, offset : Vector2) {
        val tile = RenderingUnit()
        tile.textureRegion = mappedTiles[key]
        tile.offset = offset
        tiles.add(tile)
    }

    fun getTiles() : ArrayList<RenderingUnit> {
        return tiles
    }

    override fun getRenderingUnits(): ArrayList<RenderingUnit> {
        return tiles
    }
}