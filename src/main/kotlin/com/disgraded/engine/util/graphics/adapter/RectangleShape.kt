package com.disgraded.engine.util.graphics.adapter

import com.badlogic.gdx.graphics.Pixmap
import com.badlogic.gdx.graphics.Texture
import com.badlogic.gdx.graphics.g2d.TextureRegion
import com.badlogic.gdx.math.Vector2
import com.disgraded.engine.util.graphics.Color
import com.disgraded.engine.util.graphics.GraphicAdapter
import com.disgraded.engine.util.graphics.RenderingUnit
import java.util.ArrayList

class RectangleShape(size : Vector2, color : Color) : GraphicAdapter() {

    private val renderingUnit: RenderingUnit = RenderingUnit()
    private val unitList : ArrayList<RenderingUnit> = arrayListOf(renderingUnit)

    init {
        val map = Pixmap(size.x.toInt(), size.y.toInt(), Pixmap.Format.RGBA4444)
        map.setColor(color.getColor())
        map.fillRectangle(0, 0, size.x.toInt(), size.y.toInt())
        renderingUnit.textureRegion = TextureRegion(Texture(map))
        map.dispose()
    }

    fun getRenderingUnit() : RenderingUnit {
        return renderingUnit
    }

    override fun getRenderingUnits(): ArrayList<RenderingUnit> {
        return unitList
    }
}