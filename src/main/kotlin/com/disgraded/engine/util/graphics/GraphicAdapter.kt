package com.disgraded.engine.util.graphics

import java.util.*

abstract class GraphicAdapter {

    abstract fun getRenderingUnits() : ArrayList<RenderingUnit>

}