package com.disgraded.engine.util.input

import com.badlogic.gdx.math.Vector2

class EllipseShape(var size : Vector2) : InputShape {

    private var position : Vector2 = Vector2(0f , 0f)
    var offset : Vector2 = Vector2(0f, 0f)

    constructor(size: Vector2, offset : Vector2) : this(size) {
        this.offset = offset
    }

    override fun contains(pointer: Vector2): Boolean {
        val x = pointer.x - (position.x + offset.x)
        val y = pointer.y - (position.y + offset.y)
        return x * x / (size.x * 0.5f * size.x * 0.5f) + y * y / (size.y * 0.5f * size.y * 0.5f) <= 1.0f
    }

    override fun update(position: Vector2) {
        this.position.x = position.x
        this.position.y = position.y
    }

    override fun getType(): InputShape.Type {
        return InputShape.Type.ELLIPSE
    }

    override fun getPosition(): Vector2 {
        return position
    }
}