package com.disgraded.engine.util.input

import com.badlogic.gdx.math.Vector2

class RectangleShape(var size : Vector2) : InputShape {

    private var position : Vector2 = Vector2(0f, 0f)
    var offset : Vector2 = Vector2(0f, 0f)

    constructor(size: Vector2, offset : Vector2) : this(size) {
        this.offset = offset
    }

    override fun contains(pointer: Vector2) : Boolean {
        var position = Vector2(position.x - size.x / 2 + offset.x, position.y - size.y / 2 + offset.y)
        return position.x <= pointer.x && position.x + size.x >= pointer.x
                && position.y <= pointer.y && position.y + size.y >= pointer.y
    }

    override fun update(position : Vector2) {
        this.position.x = position.x
        this.position.y = position.y
    }

    override fun getType(): InputShape.Type {
        return InputShape.Type.RECTANGLE
    }

    override fun getPosition(): Vector2 {
        return position
    }
}