package com.disgraded.engine.util.input

import com.badlogic.gdx.math.Vector2

interface InputShape {

    enum class Type { RECTANGLE, CIRCLE, ELLIPSE }

    fun contains(pointer : Vector2) : Boolean
    fun update(position : Vector2)
    fun getType() : Type
    fun getPosition() : Vector2
}