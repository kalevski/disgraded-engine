package com.disgraded.engine.util.input

import com.badlogic.gdx.math.Vector2

class CircleShape(var radius: Float) : InputShape {

    private var position : Vector2 = Vector2(0f, 0f)
    var offset : Vector2 = Vector2(0f, 0f)

    constructor(radius: Float, offset : Vector2) : this(radius) {
        this.offset = offset
    }

    override fun contains(pointer: Vector2): Boolean {
        val x = position.x - pointer.x + offset.x
        val y = position.y - pointer.y + offset.y
        return x * x + y * y <= radius * radius
    }

    override fun update(position: Vector2) {
        this.position.x = position.x
        this.position.y = position.y
    }

    override fun getType(): InputShape.Type {
        return InputShape.Type.CIRCLE
    }

    override fun getPosition(): Vector2 {
        return position
    }
}