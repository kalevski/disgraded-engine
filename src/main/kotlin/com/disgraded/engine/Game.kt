package com.disgraded.engine

import com.badlogic.gdx.ApplicationListener

abstract class Game : ApplicationListener {

    val engine : Engine = Engine.getInstance()

    override fun create() {
        initialize()
    }

    abstract fun initialize()

    override fun render() {
        engine.update()
    }

    override fun pause() {

    }

    override fun resume() {

    }

    override fun resize(width: Int, height: Int) {
        engine.resize(width, height)
    }

    override fun dispose() {
        destroy()
        engine.destroy()
    }

    abstract fun destroy()

}