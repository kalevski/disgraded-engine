package com.disgraded.engine.adapter

import com.badlogic.ashley.core.EntityListener
import com.disgraded.engine.Engine
import com.disgraded.engine.Root

abstract class EntityListener : EntityListener {

    val root : Root = Engine.getInstance().root

    override fun entityAdded(entity: com.badlogic.ashley.core.Entity?) {
        added(entity as Entity)
    }

    override fun entityRemoved(entity: com.badlogic.ashley.core.Entity?) {
        removed(entity as Entity)
    }

    abstract fun added(entity: Entity)

    abstract fun removed(entity: Entity)
}