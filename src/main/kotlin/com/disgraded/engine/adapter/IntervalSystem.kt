package com.disgraded.engine.adapter

import com.badlogic.ashley.core.Family
import com.badlogic.ashley.systems.IntervalIteratingSystem
import com.disgraded.engine.Engine

abstract class IntervalSystem(family : Family, private val interval : Float) : IteratingSystem(family) {

    private var accumulator : Float = 0f

    override fun update(deltaTime: Float) {
        accumulator += deltaTime
        while(accumulator >= interval) {
            accumulator -= interval
            exec(deltaTime)
        }
    }
}