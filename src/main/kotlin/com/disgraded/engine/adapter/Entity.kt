package com.disgraded.engine.adapter

import com.badlogic.ashley.core.Component
import com.badlogic.ashley.signals.Signal

abstract class Entity : com.badlogic.ashley.core.Entity() {

    val onCreate : Signal<Entity> = Signal()
    val onDestry : Signal<Entity> = Signal()

    fun render(deltaTime: Float) {
        update(deltaTime)
    }

    fun build() {
        onCreate.dispatch(this)
        create()
    }

    fun destroy() {
        onDestry.dispatch(this)
        dispose()
        this.removeAll()
    }

    override fun add(component: Component): com.badlogic.ashley.core.Entity {
        if ((component as com.disgraded.engine.adapter.Component).onAdd(this)) {
            return super.add(component)
        } else {
            return this
        }
    }

    override fun remove(componentClass: Class<out Component>): Component {
        if ((getComponent(componentClass) as com.disgraded.engine.adapter.Component).onRemove(this)) {
            return super.remove(componentClass)
        } else {
            return getComponent(componentClass)
        }
    }

    abstract fun create()

    abstract fun update(deltaTime : Float)

    abstract fun dispose()
}