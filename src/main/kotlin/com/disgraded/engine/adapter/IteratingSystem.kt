package com.disgraded.engine.adapter

import com.badlogic.ashley.core.Family
import com.badlogic.ashley.systems.IteratingSystem
import com.disgraded.engine.Engine

// TODO: implement handler for changes in filter method

abstract class IteratingSystem(family : Family) : BasicSystem(family) {

    override fun update(deltaTime: Float) {
        exec(deltaTime)
    }

    abstract fun before(entityList : ArrayList<Entity>, deltaTime: Float)

    abstract fun each(entity: Entity, deltaTime: Float)

    abstract fun after(entityList : ArrayList<Entity>, deltaTime: Float)

    protected fun exec(deltaTime: Float) {
        val entityList: ArrayList<Entity> = getList()
        before(entityList, deltaTime)
        for (entity in entityList) {
            each(entity, deltaTime)
        }
        after(entityList, deltaTime)
    }

}