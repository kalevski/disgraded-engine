package com.disgraded.engine.adapter

import com.badlogic.ashley.core.Family
import com.badlogic.ashley.systems.IteratingSystem
import com.disgraded.engine.Engine

abstract class BasicSystem(family : Family) : IteratingSystem(family) {

    val engine : Engine = Engine.getInstance()
    private val entityList : ArrayList<Entity> = arrayListOf()

    override fun update(deltaTime: Float) {}


    override fun addedToEngine(engine: com.badlogic.ashley.core.Engine?) {
        added()
        super.addedToEngine(engine)
    }

    override fun removedFromEngine(engine: com.badlogic.ashley.core.Engine?) {
        removed()
        super.removedFromEngine(engine)
    }

    abstract fun added()

    abstract fun filter(entityList : ArrayList<Entity>) : ArrayList<Entity>

    override fun processEntity(entity: com.badlogic.ashley.core.Entity?, deltaTime: Float) {}

    abstract fun removed()

    fun getList() : ArrayList<Entity> {
        entityList.clear()
        for (entity in entities) {
            entityList.add(entity as Entity)
        }
        return filter(entityList)
    }

}