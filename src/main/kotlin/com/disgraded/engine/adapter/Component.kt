package com.disgraded.engine.adapter

import com.badlogic.ashley.core.Component
import com.disgraded.engine.Engine
import com.disgraded.engine.Root

abstract class Component : Component {

    val root : Root = Engine.getInstance().root

    abstract fun onAdd(entity: Entity) : Boolean

    abstract fun onRemove(entity: Entity) : Boolean

}