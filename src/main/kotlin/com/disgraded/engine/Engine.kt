package com.disgraded.engine

import com.badlogic.gdx.Gdx
import com.badlogic.gdx.graphics.GL20
import com.badlogic.gdx.math.Vector2
import com.disgraded.engine.manager.*
import com.disgraded.engine.module.GraphicsModule
import com.disgraded.engine.module.PhysicsModule
import com.disgraded.engine.util.Config

class Engine {

    companion object {
        private val instance = Engine()

        @Synchronized
        fun getInstance(): Engine {
            return instance
        }
    }

    val graphics : GraphicsModule = GraphicsModule(this)
    val physics : PhysicsModule = PhysicsModule(this)

    val root = Root(this)

    val windowSize : Vector2 = Vector2(0f, 0f)

    init {
        println("engine initialized!")
    }

    fun init(config : Config) {
        root.init(config)
    }

    fun update() {
        root.debug.begin("game")
        Gdx.gl.glClear(GL20.GL_COLOR_BUFFER_BIT)
        Gdx.gl.glClearColor(0f, 0f ,0f, 1f)
        val deltaTime = Gdx.graphics.deltaTime
        root.update(deltaTime)
        root.debug.end("game")
    }

    fun resize(width: Int, height: Int) {
        windowSize.set(width.toFloat(), height.toFloat())
        graphics.resize(windowSize)
        println("resized: $width : $height")
    }

    fun destroy() {
        root.dispose()
        println("engine destroyed!")
    }
}