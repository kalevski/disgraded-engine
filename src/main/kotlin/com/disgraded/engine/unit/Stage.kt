package com.disgraded.engine.unit

import com.disgraded.engine.Engine
import com.disgraded.engine.Root
import com.disgraded.engine.util.Config

abstract class Stage {

    val root : Root = Engine.getInstance().root

    abstract fun configure() : Config?
    abstract fun create()
    abstract fun update()
    abstract fun dispose()
}