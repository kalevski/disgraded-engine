package com.disgraded.engine.unit

import com.disgraded.engine.adapter.Entity
import com.disgraded.engine.component.CoreComponent

abstract class GameObject : Entity {

    constructor() {
        add(CoreComponent())
    }

}