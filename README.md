# disgraded-engine
Game engine based on libGDX game framework

## Getting started
1. Create standard libGDX project with Kotlin support
```
// gradle config (build.gradle)
dependencies {
  implementation "com.disgraded.engine" // soon
}
```

## Roadmap
### v.0.5.0
- Simple graphics render (Implemented)
- Physics (Box2D) support (In Progress)
- Input handler (Implemented)
- Sound handler
- Spritesheet support (Implemented)
- Polygon graphics support (Implemented)
- Multiple camera support
- Input debugger (Implemented)
- Event system
- Store
